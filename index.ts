const express = require('express')
const Router = require('./src/api')
const config = require('config')
const fs =require('fs')
// const fileupload = require("express-fileupload");

const App = express()

// App.use(fileupload({
//     createParentPath: true
// }));
App.use(express.json())
App.use(express.urlencoded())

if (!config.get('jwtPrivateKey')) {
    console.error("FATAL ERROR: jwtPrivateKey is not defiend");
    process.exit(1)
}

App.use(Router);


App.listen(300, () => {
    console.log("start listening to port 300 ... ");
})
