const fs = require('fs')
const {get} = require('../../services/file')
const storPath = '../../../data/'
const getFile = (req,res)=>{
    try{
        const {fileId} = req.params
        const {uId} = req.params
        const file = get(uId,fileId)
        res.contentType(file.type);
        res.setHeader('Content-disposition', 'attachment; filename=' + file.name);
        const filestream = fs.createReadStream(storPath+file.storName);
        filestream.pipe(res);

    }catch(err){
        console.log(err)
        if(err._code)
            return res.status(err.status).send(err.message)
        res.status(500).send('internal server error')
        console.log(err)
    }
}
module.exports = getFile