const fileService = require('../../services/file')
const upload=async(req,res)=>{
    try{
        const {file} = req
        if(!file)
            throw {_code:1,message:"file is required",status:400}
        const {folderId,name,isPublic} = req.params
        const {uId} = req.currUser
        
        const result = await fileService.create(uId,file,name,folderId,isPublic)
        res.send(result)
    }catch(err){
        console.log(err)
        if(err._code)
            return res.status(err.status).send(err.message)
        res.status(500).send('internal server error')
        console.log(err)
    }

}
module.exports = upload;