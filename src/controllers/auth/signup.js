const userService = require('../../services/auth')
const signup = async(req,res)=>{
    try{

        const {userName,password,email} = req.body

        const result = await userService.signup(userName,email,password)
        
        res.send({...result,authentication_string:undefined})

    }catch(err){
        console.log(err)
        if(err._code)
            return res.status(err.status).send(err.message)
        res.status(500).send('internal server error')
        console.log(err)
    }

}


module.exports = signup