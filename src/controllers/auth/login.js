const userService = require('../../services/auth')
const login = async(req,res)=>{
    try{

        const {password,email} = req.body

        const result = await userService.login(email,password)
        
        res.send(result)

    }catch(err){
        console.log(err)
        if(err._code)
            return res.status(err.status).send(err.message)
        res.status(500).send('internal server error')
        console.log(err)
    }

}


module.exports = login