const create = require('./create')
const getFolderContent = require('./getFolderContent')
module.exports={
    create,
    getFolderContent
}