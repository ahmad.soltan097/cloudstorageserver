const {getFolderContent} = require('../../services/folder')

const get = async(req,res)=>{
    try {
    const {folderId,offset,limit} = req.params
    const {uId, rootFolderId } = req.currUser
    const result = await getFolderContent(uId,folderId||rootFolderId,[],offset||0,limit||20)
    res.send(result)
    }catch (err) {
        console.log(err)
        if (err._code)
            return res.status(err.status).send(err.message)
        res.status(500).send('internal server error')
        console.log(err)
    }
}
module.exports = get