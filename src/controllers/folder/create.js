const folderService = require('../../services/folder')

const create = async (req, res) => {
    try {
        const { name, isPublic, folderId } = req.body
        const { uId, rootFolderId } = req.currUser
        const result = await folderService
            .create(
                uId,
                folderId || rootFolderId,
                name,
                isPublic || false)
        res.send(result)
    } catch (err) {
        console.log(err)
        if (err._code)
            return res.status(err.status).send(err.message)
        res.status(500).send('internal server error')
        console.log(err)
    }
}
module.exports = create