
const express = require('express');
const router = express.Router()
const folderController = require('../controllers/folder')
const {checkToken} = require('../services/auth')

router.post('/create',checkToken(),folderController.create)
router.get('/get/:folderId?/:offset?/:limit?',checkToken(),folderController.getFolderContent)
module.exports = router;
