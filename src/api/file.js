const express = require('express');
const router = express.Router()
const multer = require('multer')
const upload = multer({limits: {fileSize: 2000000 },dest:'uploads/'})
const fileController = require('../controllers/file')
const {checkToken} = require('../services/auth')

router.post('/upload/:folderId/:name/:isPublic?',checkToken(),upload.single('file'),fileController.upload)
router.get('/get/:fileId',checkToken(),fileController.get)

module.exports =router

