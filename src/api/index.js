const express = require('express')
const authRouter = require('./auth')
const fileRouter = require('./file')
const folderRouter = require('./folder')

const router = express.Router()

const reqLogger=(req,res,next)=>{
    console.log(req.originalUrl)
    next()
}

router.use("/",reqLogger)
router.use("/api/auth", authRouter);
router.use("/api/file", fileRouter);
router.use("/api/folder", folderRouter);



module.exports = router;