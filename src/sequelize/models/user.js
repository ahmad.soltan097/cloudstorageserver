'use strict';
const {
  Model
} = require('sequelize');
const { Op } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static isExistUserName(userName) {
      return this.findOne({ where: { userName } })
    }
    static isExistEmail(email) {
      return this.findOne({ where: { email } })
    }
    static setRootFolder(uId,rootFolderId){
      return this.update({rootFolderId},{
        where:{
          id:uId
        }
      })
    }
    static findUserByUserNameOrEmail(email) {
      return this.findOne({
        where: {
          [Op.or]: [
            { userName: email },
            { email }
          ]
        }
      })
    }
    static associate(models) {
      // define association here
      this.hasMany(models.Folder)
      this.hasMany(models.SharedFolder)
    }
  };
  User.init({
    userName: DataTypes.STRING,
    authentication_string: DataTypes.STRING,
    email: DataTypes.STRING,
    rootFolderId: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};