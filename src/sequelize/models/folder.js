'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Folder extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static counFolderContent(folderId){
      return this.count({where:{
        parentId:folderId
      }})
    }
    static getFilesByFolderId(folderId,order=[],offset=0,limit=20){
      console.log('limit',limit)
      console.log('offset',offset)
      return this.findAll({
        where:{
          parentId:folderId
        },
        limit,
        offset,
        order
      });
    }
    static associate(models) {
      // define association here
    }
  };
  Folder.init({
    name: DataTypes.STRING,
    UserId: DataTypes.INTEGER,
    isPublic: DataTypes.BOOLEAN,
    type:DataTypes.STRING,
    parentId:DataTypes.INTEGER,
    storName:DataTypes.STRING,
    path:DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Folder',
  });
  return Folder;
};