'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SharedFolder extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static getUserToFolderShareRecord(uId,folderIds){
      return this.findOne({
        where:{
          folderId:{
            [Op.in]:folderIds
          },
          UserId:uId
        }
      })
    }
    static associate(models) {
      // define association here
    }
  };
  SharedFolder.init({
    UserId: DataTypes.INTEGER,
    folderId: DataTypes.INTEGER,
    roles: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'SharedFolder',
  });
  return SharedFolder;
};