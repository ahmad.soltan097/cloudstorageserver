
const  { v4 : uuidv4 } = require('uuid');
const fs = require('fs');
const storPath = '../../../data/'
if (!fs.existsSync(storPath)){
    fs.mkdirSync(storPath);
}

const upload = async(file)=>{
    storName = uuidv4()
    console.log('copy start')
    await copyFile(file.path,storPath+storName)
    console.log('copy done')
    return storName
}
const copyFile=(filePath,destPath)=>{
    return new Promise((res,rej)=>{
        fs.copyFile(filePath,destPath,(err)=>{
            if(err)
                return rej()
            res(true)   
        })
    })
}
module.exports = upload