const Folders= require('../../sequelize/models').Folder
const {canAccessFolder} = require('../folder')
const upload = require('./upload')
const create = async(uId,file,name,folderId,isPublic)=>{
    console.log(file.path)
    await canAccessFolder(uId,Number(folderId),['C'])
    const storName = await upload(file)
    const result = (await Folders.create({
        name,
        storName,
        UserId: uId,
        type:file.mimetype,
        parentId:folderId,
        isPublic
    })).dataValues
    return result
}
module.exports = create
