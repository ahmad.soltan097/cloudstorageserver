const {canAccessFolder} = require('../folder')
const {Folder:Folders} = require('../../sequelize/models')
const get = async(uId,fileId)=>{
    await canAccessFolder(uId,Number(fileId),['R'])
    const file = await Folders.findByPk(fileId)
    return file
}
module.exports = get