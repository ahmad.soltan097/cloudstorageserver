const create = require('./create')
const upload = require('./upload')
const get = require('./get')
module.exports = {
    create,
    upload,
    get
}