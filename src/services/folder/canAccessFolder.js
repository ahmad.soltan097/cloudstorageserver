const folders = require('../../sequelize/models').Folder
const SharedFolders = require('../../sequelize/models').SharedFolder

const canAccessFolder = async (uId, folderId, reqRoles = []) => {
    const info = await folders.findByPk(folderId)
    if (!info)
        throw { _code: 1, message: 'folder not found', status: 401 }
    if (info.UserId != uId && !info.isPublic) {
        const folderIds = info.path.split('/').map(v=>(Number(v)))
        const record = await SharedFolders.getUserToFolderShareRecord(uId, folderIds)
        if (!!record)
            throw { _code: 1, message: "can't Access this folder" }
        var roles = JSON.parse(record.roles)
        if (!checker(roles, reqRoles))
            throw { _code: 1, message: "permission denied", status: 401 }
    }
    return {info,roles}
}

const checker = (arr, target) => target.every(v => arr.includes(v));


module.exports = canAccessFolder