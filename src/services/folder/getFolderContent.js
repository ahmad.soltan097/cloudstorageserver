const Folders = require('../../sequelize/models').Folder
const canAccessFolder = require('./canAccessFolder')
const get = async (uId, folderId, order = [], offset = 0, limit = 20) => {
    await canAccessFolder(uId, folderId, ['R']);
    const result = await Folders.getFilesByFolderId(Number(folderId), order, Number(offset), Number(limit))
    const count = await Folders.counFolderContent(Number(folderId))
    return {count ,data:result};
}

module.exports = get