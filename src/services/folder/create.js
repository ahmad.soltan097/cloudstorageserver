const Folders = require('../../sequelize/models').Folder
const canAccessFolder = require('./canAccessFolder')
const create = async (uId,folderId,name,isPublic) => {
    if(!!folderId)
        var {info} = await canAccessFolder(uId,Number(folderId),['C'])
    const folder = (await Folders.create({
        name,
        parentId:Number(folderId),
        UserId: uId,
        type: 'dir',
        path:!!folderId?((!!info.path?(`${info.path}/`):'')+`${info.id}`):'',
        isPublic
    })).dataValues
    return folder
}
module.exports = create
