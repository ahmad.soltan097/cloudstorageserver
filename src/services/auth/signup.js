
const bcrypt = require('bcrypt')
const Users = require('../../sequelize/models').User
const {create} = require('../folder')
const signup = async (userName, email, _password) => {
    if (!!await Users.isExistUserName(userName))
        throw { _code: 1, message: "used username", status: 401 };
    if (!!await Users.isExistEmail(email))
        throw { _code: 2, message: "used email", status: 401 };
    
    const password = await encode(_password, 10)
    const result = (await Users.create({
        userName,
        email,
        authentication_string: password
    })).dataValues
    const rootFolder = await create(result.id,'','root',false)
    await Users.setRootFolder(result.id,rootFolder.id)

    return {
        id: result.id,
        userName: result.userName,
        email: result.email,
        rootFolder
    }
}
const encode = (data, num) => {
    return new Promise((res, rej) => {
        bcrypt.hash(data, num, (err, hash) => {
            if (err)
                return rej()
            res(hash)
        })
    })
}
module.exports = signup
// signup('alyysi3s33', 'ahsssmad', "123").then(data => console.log(data)).catch(err => console.log(err))
