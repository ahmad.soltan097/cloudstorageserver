const signup = require('./signup')
const login = require('./login');
const checkToken = require('./checkToken');
module.exports = {
    signup,
    login,
    checkToken
}