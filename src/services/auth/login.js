
const bcrypt = require('bcrypt')
const config = require('config')
const jwt = require('jsonwebtoken');
const Users = require('../../sequelize/models').User
const login = async(email,_password)=>{
    let user = await Users.findUserByUserNameOrEmail(email)
    if(!user)
        throw {_code:1,message:"invalid user",status:400};
    const isValid =await bcrypt.compare(_password,user.authentication_string)
    if(!isValid)
        throw {_code:1,message:"invalid password",status:400};

    return {
        id:user.id,
        userName:user.userName,
        email:user.email,
        token:genToken({uId:user.id,rootFolderId:user.rootFolderId})
    }
}

const genToken = (data) => {
    const token = jwt.sign(data, config.get("jwtPrivateKey"))
    return token
}

module.exports = login
// login('alyysi3s33',"123").then(data=>console.log(data)).catch(err=>console.log(err))
