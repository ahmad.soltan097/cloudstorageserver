
const config = require('config')
const key = config.get("jwtPrivateKey") 
const jwt = require('jsonwebtoken');

const checkToken = (opptions={}) =>
    async(req, res, next) => {
        opptions={isVerifiedReq:true,...opptions}
        const { token } = req.headers
        try {
            req.currUser = jwt.verify(token || "K", key)
            console.log('currUser', req.currUser)
            next()
        } catch (err) {
            console.log(err)
            return res.status(401).send('auth error');
            
        }
    }

module.exports = checkToken